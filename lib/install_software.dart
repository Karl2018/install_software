import 'dart:async';

import 'package:flutter/services.dart';

class InstallSoftware {
  static const MethodChannel _channel = const MethodChannel('install_software');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> installSoftware(String path) async {
    print("in dart:"+path);
    final String isSuccess = await _channel
        .invokeMethod('install_apk', <String, dynamic>{"apk_path": path});
    return isSuccess;
  }
}
